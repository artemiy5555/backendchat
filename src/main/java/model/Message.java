package model;

import javax.persistence.*;
import java.sql.Time;

@Entity
@Table(name = "message")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_message")
    private int id_message;
    @Column(name = "text")
    private String text;
    @Column(name = "nameOfChat")
    private String nameOfChat;
    @Column(name = "time")
    private String time;
    @Column(name = "user")
    private String user;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "user")
//    private User user;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "nameOfChat",insertable=false, updatable=false)
//    private Chat chat ;


    public Message(int id_message,String text, String nameOfChat, String time, String user) {
        this.id_message = id_message;
        this.text = text;
        this.nameOfChat =nameOfChat;
        this.time = time;
        this.user = user;

    }

//    public Message(int id_message,String text, Chat nameOfChat, String time, User user) {
//        this.id_message = id_message;
//        this.text = text;
//        this.nameOfChat = nameOfChat.getNameChat();
//        this.time = time;
//        this.user = user.getLogin();
//    }

    public Message() {
    }



    public int getId_message() {
        return id_message;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

//    public void setId_message(int id_message) {
//        this.id_message = id_message;
//    }


    public String getNameOfChat() {
        return nameOfChat;
    }

    public void setNameOfChat(String nameOfChat) {
        this.nameOfChat = nameOfChat;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id_message=" + id_message +
                ", text='" + text + '\'' +
                ", nameOfChat='" + nameOfChat + '\'' +
                ", time='" + time + '\'' +
                ", user='" + user + '\'' +
                '}';
    }
}
