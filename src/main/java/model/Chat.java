package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "chat")
public class Chat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_chat")
    private int id_chat;
    @Column(name = "name")
    private String nameChat;
    @Column(name = "created_at")
    private String created_at;
    @Column(name = "update_at")
    private String update_at;
    @Column(name = "is_private")
    private Boolean is_private;
    @ManyToMany(mappedBy = "chats")
    private Set<User> users;

    public Boolean getIs_private() {
        return is_private;
    }

    public void setIs_private(Boolean is_private) {
        this.is_private = is_private;
    }
//    @OneToMany(mappedBy = "chat", cascade = CascadeType.ALL, orphanRemoval = true)
//    private List<Message> messages;


//    public List<Message> getMessages() {
//        return messages;
//    }

//    public void setMessages(List<Message> messages) {
//        this.messages = messages;
//    }


    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

//    public void addMes(Message message){
//        messages.add(message);
//    }
//
//    public void removeMes(Message message){
//        messages.remove(message);
//    }

    public void addUser(User user){
        users.add(user);
    }

    public void removeUser(User user){
        users.remove(user);
    }

    public Chat(int id_chat, String nameChat,boolean is_private,String created_at,String update_at) {
        this.id_chat = id_chat;
        this.nameChat = nameChat;
        this.is_private = is_private;
        this.created_at =created_at;
        this.update_at = update_at;
//        messages = new ArrayList<>();
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

    public Chat() {
    }

    public int getId_chat() {
        return id_chat;
    }

    public String getNameChat() {
        return nameChat;
    }

    public void setNameChat(String nameChat) {
        this.nameChat = nameChat;
    }

    @Override
    public String toString() {
        return "Chat{" +
                "id_chat=" + id_chat +
                ", nameChat='" + nameChat +
//                ", message='" + messages +
                '}';
    }
}
